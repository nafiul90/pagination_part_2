import React from 'react';
import { useLocation } from 'react-router-dom';
import { Post } from '../models/Post';

const Details: React.FC = () => {
    const {state} = useLocation();
    const post = state as Post;
    return (
        <div style={{textAlign: 'center'}} data-testid="details" >
            <h3 data-testid="text">Details</h3>
            <pre>{JSON.stringify(post, null, 2)}</pre>

        </div>
    );
}
export default Details;
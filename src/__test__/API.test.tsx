import { getPaginationData } from "../services/PostService";

describe("Api call test", () => {
  test("should render api data",async () => {
    const res = await getPaginationData(0);
    expect(res).toBeDefined();
   });
})
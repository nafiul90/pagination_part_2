import {render, screen } from "@testing-library/react";
import Home from "../components/Home";

describe("Home component testing", () => {

  test("should render <Home /> component", () => {
    render(<Home />);
    const linkElement = screen.getByTestId("home");
    expect(linkElement).toBeInTheDocument();
  });

});
export interface Column{
    id: 'title' | 'url' | 'created_at' | 'author';
    label: string;
    minWidth?: number;
    align?: "right";
}


export interface Post{
    title: string;
    url: string;
    created_at: Date;
    author: string;
}